//项目中的蓝牙代码

let respData = []
let dataSendLen = 0 //帧发送的长度
let allMsgArr = [] //所有需要发送的数据，按长度不超17字节截取在这里
let getMsgFlag = true //每次发送是否已经收到了返回消息
let startSendIndex = 0 //发送消息时从哪一帧开始发送
let eachRespData = [] //每次接收的帧返回来的消息
export class BlueData {
  constructor() {
  }

  sendData(deviceId, sendData) {
    console.log("sendData")
    let msgArr = []
    dataSendLen = 0
    respData = []
    for (let i = 0; i < sendData.length; i = i + 34) {
      msgArr.push(sendData.substr(i, 34))
    }
    dataSendLen = msgArr.length
    allMsgArr = msgArr
    startSendIndex = 0
    this.continueSend(deviceId)
  }

  // 接到sendData的数据发送
  continueSend(deviceId) {
    // alert('接到sendData的数据发送')
    if (startSendIndex > dataSendLen - 1) {
      return
    }
    if (getMsgFlag) {
      console.log("continueSend")
      getMsgFlag = false
      // 发送之前转换buffer
      let tranMsg = this.str2ab(allMsgArr[startSendIndex], startSendIndex, allMsgArr)
      console.log(this.buffer2hex(tranMsg) + 'tranMsg')
      // 蓝牙服务ID
      let serviceId = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
      let characteristicId = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E'
      return new Promise((resolve, reject) => {
        plus.bluetooth.writeBLECharacteristicValue({
          deviceId: deviceId,
          serviceId: serviceId,
          characteristicId: characteristicId,
          value: tranMsg,
          success: function (e) {
            console.log(e)
            resolve(e)
          },
          fail: function (e) {
            console.log(e)
            reject(e)
          },
          complete: function (e) {
            console.log(e)
          }
        })
      })
    }
  }

  str2ab(str, index, msgArr) {
    let strByteLen = str.length / 2
    let buf = new ArrayBuffer(strByteLen + 3)
    let dataView = new DataView(buf)
    dataView.setUint8(0, 0x4a) //0x4a
    dataView.setUint8(1, this.bit2ForHexadecimal(index, msgArr))
    //这里把长度转换成16进制数
    let hexLen = parseInt(strByteLen, 10).toString(16)
    if (hexLen.length == 1) {
      hexLen = '0' + hexLen
    }
    dataView.setUint8(2, '0x' + hexLen)
    for (let i = 0, strLen = str.length; i < strLen; i = i + 2) {
      let n = str.substr(i, 2)
      if (n.length == 1) {
        n = '0' + n
      }
      dataView.setUint8(i / 2 + 3, '0x' + n)
    }
    return buf
  }

  /**
   * 读取的数据 格式转换
   * @param byte数组
   * @returns {string}
   */
  buffer2hex(buffer) {
    var hexArr = Array.prototype.map.call(new Uint8Array(buffer), function (bit) {
      return ('00' + bit.toString(16)).slice(-2)
    })
    return hexArr.join('')
  }

  //生成控制字,index为第几帧数据，返回一个二时制的数据
  bit2ForHexadecimal(index, msgArr) {
    let baseBit = 0b00000000
    let ctrlBit = 0
    let i = index + 1
    if (i == 1) {
      //如果第一帧，高位7为1，其它位为0；
      ctrlBit = 0b10000000
    }
    if (msgArr.length == i) {
      //如果是最后一帧，高位为0，bit6为1，最低帧为顺序加1
      ctrlBit = ctrlBit ^ (0b01000000 ^ (i - 1))
    } else {
      if (ctrlBit) {
        //如果第一帧的话，最低位要顺序加1
        ctrlBit = ctrlBit ^ (baseBit ^ (i - 1))
      } else {
        ctrlBit = baseBit ^ (i - 1)
      }
    }
    //ctrlBit会自动变成10进制
    //这里把10进制的ctrlBit转成16进制
    let ctrlBit16 = parseInt(ctrlBit, 10).toString(16)
    if (ctrlBit16.length < 2) {
      ctrlBit16 = '0x0' + ctrlBit16
    } else {
      ctrlBit16 = '0x' + ctrlBit16
    }
    return ctrlBit16
  }

  connectDevice(address) {
    return new Promise((resolve, reject) => {
      plus.bluetooth.openBluetoothAdapter({
        success: (e) => {
          console.log(e)
          plus.bluetooth.createBLEConnection({
            deviceId: address,
            success: (e) => {
              console.log(e)
              this.readData(address)
              resolve(e)
            },
            fail: function (e) {
              console.log(e)
              reject(e)
            },
            complete: function (e) {
              console.log(e)
            }
          })
        },
        fail: function (e) {
          reject(e)
        }
      })
    })
  }

  /**
   * 读取数据
   */
  readData(deviceId) {
    // alert('读取数据')
    setTimeout(function () {
      plus.bluetooth.notifyBLECharacteristicValueChange({
        deviceId: deviceId,
        serviceId: '6E400001-B5A3-F393-E0A9-E50E24DCCA9E',
        characteristicId: '6E400003-B5A3-F393-E0A9-E50E24DCCA9E',
        success: function (e) {
          console.log('readData ===== success: ' + JSON.stringify(e))
        },
        fail: function (e) {
          console.log('readData ===== failed: ' + JSON.stringify(e))
        },
        complete: function (e) {
          console.log(JSON.stringify(e) + 'complete')
        }
      })
    }, 2000)
    let setIntervalId = setInterval(function () {
      // 监听低功耗蓝牙设备的特征值变化
      plus.bluetooth.onBLECharacteristicValueChange(function (e) {
        console.log(e)
        // alert(JSON.stringify(e) + '定时e jinlai le ')
        if (e.value != undefined) {
          // alert(buffer2hex(e.value))
          let endFlag = this.confirm4b(e.value, deviceId)
          // alert(endFlag + JSON.stringify(endFlag) + '=========endFlag')
          if (endFlag == '4b') {
            startSendIndex++
            this.continueSend(deviceId)
          } else if (endFlag == 'end') {
            // alert(buffer2hex(e.value) + 'e.value1')
            respData.push(eachRespData.join(''))
            eachRespData = []
            e.value = respData.join('')
            // alert(e.value + '-------------e.valyue' + typeof e.value)
            if (e.value != '') {
              localStorage.setItem('fhbw', e.value)
            }
          } else if (endFlag == 'oneEnd') {
            // alert(buffer2hex(e.value) + 'e.value2')
            return
          }
        }
      })
    }, 40)
  }

  confirm4b(value, deviceId) {
    let dataView = new DataView(value)
    let firstBF = dataView.getUint8(0)
    firstBF = parseInt(firstBF, 10).toString(16)
    firstBF = firstBF.toLocaleLowerCase()
    let secondBF = dataView.getUint8(1)
    //如果接收到的是4b确认帧
    if (firstBF.indexOf('4b') > -1) {
      getMsgFlag = true
      return '4b'
    } else {
      for (var i = 0; i < value.byteLength; i++) {
        if (i >= 3) {
          var bf = dataView.getUint8(i)
          let rbf = bf.toString(16)
          if (rbf.length == 1) {
            rbf = '0' + rbf
          }
          eachRespData.push(rbf)
        }
      }
      //ctrl如果是以11 或 01 表示结束
      let binSecondBF = parseInt(secondBF, 10).toString(2)
      let binSecondBFLen = binSecondBF.length
      //如果少于8位，在前面补0
      if (binSecondBFLen < 8) {
        binSecondBF = '00000000'.substr(0, 8 - binSecondBFLen) + binSecondBF
      }
      let bit6 = binSecondBF.substr(1, 1)
      getMsgFlag = true
      //如果第6位是1表示传输结束
      if (bit6 == 1) {
        //发送确认消息
        this.confirmSend(secondBF, deviceId)
        return 'end'
      }
      //发送确认消息
      this.confirmSend(secondBF, deviceId)
      return 'oneEnd'
    }
  }

  confirmSend(secondBF, deviceId) {
    let cbf = new ArrayBuffer(3)
    let dataView = new DataView(cbf)
    dataView.setUint8(0, 0x4b)
    let ctrl = secondBF | 0b00100000
    let ctrl16 = parseInt(ctrl, 10).toString(16)
    if (ctrl16.length == 1) {
      ctrl16 = '0' + ctrl16
    }
    dataView.setUint8(1, '0x' + ctrl16)
    dataView.setUint8(2, 0x00)
    var serviceId = '6E400001-B5A3-F393-E0A9-E50E24DCCA9E'
    var characteristicId = '6E400002-B5A3-F393-E0A9-E50E24DCCA9E'
    plus.bluetooth.writeBLECharacteristicValue({
      deviceId: deviceId,
      serviceId: serviceId,
      characteristicId: characteristicId,
      value: cbf,
      success: function (res) {
        console.log('发送确认指令成功')
        console.log(res)
      },
      fail: function (res) {
        console.warn('发送确认指令失败', res)
      }
    })
  }
}
