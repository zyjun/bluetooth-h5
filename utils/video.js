// 选择视频功能
export function chooseVideoFile() {
  if (!isPlusClient()) {
    console.error("不是plus环境")
    return
  }
  return new Promise((resolve, reject) => {
    let cmr = plus.camera.getCamera()
    cmr.startVideoCapture(
        function (p) {
          plus.io.resolveLocalFileSystemURL(
              p,
              function (entry) {
                compressVideo(entry.toLocalURL())
              },
              function (e) {
                plus.nativeUI.toast('读取摄像文件错误：' + e.message)
              }
          )
        },
        function (e) {
        },
        {filter: 'image'}
    )
  })
}

const isPlusClient = () => {
  if (typeof plus === 'object') return true//手机客户端
  else return false
}

// 压缩视频
function compressVideo(src) {
  plus.nativeUI.showWaiting();
  plus.zip.compressVideo(
      {
        src: src,
        quality: 'medium'
      },
      function (zip) {
        plus.nativeUI.closeWaiting();
        console.log("压缩视频成功：" + JSON.stringify(zip));
      },
      function (error) {
        plus.nativeUI.closeWaiting();
        console.error("压缩视频失败：" + JSON.stringify(error));
      });
}


